```
const role1 = new Role()
role1.name = "admin"

const role2 = new Role()
role2.name = "user"

const account = new Account()
account.username = "mmehrtens"
account.password = "sehr_sichere_sache"
account.createdAt = new Date()
account.roles = [role1, role2]

await AppDataSource.getRepository(Account).save(account)
```