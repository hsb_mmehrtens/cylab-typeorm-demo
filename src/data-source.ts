import "reflect-metadata"
import { DataSource } from "typeorm"

export const AppDataSource = new DataSource({
    type: "mariadb",
    host: "127.0.0.1",
    port: 3307,
    username: "typeorm-user",
    password: "typeorm-user-password",
    database: "typeorm-demo",
    synchronize: false,
    logging: true,
    entities: ["src/entity/**/*.ts"],
    migrations: ["src/migration/**/*.ts"],
    subscribers: ["src/subscriber/**/*.ts"],  
})
