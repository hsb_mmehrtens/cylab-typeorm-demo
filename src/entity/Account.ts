import { Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn, Unique } from "typeorm";
import { Role } from "./Role";

@Entity()
@Unique(['username'])
export class Account {

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    username: string

    @Column()
    password: string

    @Column()
    createdAt: Date

    @ManyToMany(() => Role, (role) => role.accounts)
    @JoinTable()
    roles: Role[]
    
}