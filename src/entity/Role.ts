import { Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn, Unique } from "typeorm";
import { Account } from "./Account";

@Entity()
@Unique(['name'])
export class Role {

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    name: string

    @Column('text')
    description: string

    @ManyToMany(() => Account, (account) => account.roles)
    accounts: Account[]
}