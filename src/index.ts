import { AppDataSource } from "./data-source"
import { Account } from "./entity/Account"
import { Role } from "./entity/Role"

AppDataSource.initialize().then(async () => {

    const role1 = new Role()
    role1.id = 1
    role1.name = "admin"
    role1.description = "Admin Role"

    const role2 = new Role()
    role2.id = 2
    role2.name = "user"
    role2.description = "User Role"

    //await AppDataSource.getRepository(Role).save([role1, role2]);

    const account = new Account()
    account.username = "mmehrtens"
    account.password = "sehr_sichere_sache"
    account.createdAt = new Date()
    account.roles = [role1, role2]

    await AppDataSource.getRepository(Account).save(account)

}).catch(error => console.log(error))
