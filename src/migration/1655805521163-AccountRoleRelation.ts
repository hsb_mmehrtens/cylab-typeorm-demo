import { MigrationInterface, QueryRunner } from "typeorm";

export class AccountRoleRelation1655805521163 implements MigrationInterface {
    name = 'AccountRoleRelation1655805521163'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE \`account_roles_role\` (\`accountId\` int NOT NULL, \`roleId\` int NOT NULL, INDEX \`IDX_5cbe4f53b5d73425994b0758d8\` (\`accountId\`), INDEX \`IDX_151e1faa59237afd70029aa002\` (\`roleId\`), PRIMARY KEY (\`accountId\`, \`roleId\`)) ENGINE=InnoDB`);
        await queryRunner.query(`ALTER TABLE \`account_roles_role\` ADD CONSTRAINT \`FK_5cbe4f53b5d73425994b0758d87\` FOREIGN KEY (\`accountId\`) REFERENCES \`account\`(\`id\`) ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE \`account_roles_role\` ADD CONSTRAINT \`FK_151e1faa59237afd70029aa002f\` FOREIGN KEY (\`roleId\`) REFERENCES \`role\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`account_roles_role\` DROP FOREIGN KEY \`FK_151e1faa59237afd70029aa002f\``);
        await queryRunner.query(`ALTER TABLE \`account_roles_role\` DROP FOREIGN KEY \`FK_5cbe4f53b5d73425994b0758d87\``);
        await queryRunner.query(`DROP INDEX \`IDX_151e1faa59237afd70029aa002\` ON \`account_roles_role\``);
        await queryRunner.query(`DROP INDEX \`IDX_5cbe4f53b5d73425994b0758d8\` ON \`account_roles_role\``);
        await queryRunner.query(`DROP TABLE \`account_roles_role\``);
    }

}
